<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A78782">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties farevvel speech unto the Lords Commissioners at Newport in the Isle of Wight.</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A78782 of text in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.13[51]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A78782</idno>
    <idno type="STC">Wing C2305</idno>
    <idno type="STC">Thomason 669.f.13[51]</idno>
    <idno type="EEBO-CITATION">99869822</idno>
    <idno type="PROQUEST">99869822</idno>
    <idno type="VID">162947</idno>
    <idno type="PROQUESTGOID">2240953283</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A78782)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162947)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f13[51])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties farevvel speech unto the Lords Commissioners at Newport in the Isle of Wight.</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1648]</date>
     </publicationStmt>
     <notesStmt>
      <note>With engraving of royal seal at head of document.</note>
      <note>Imprint from Wing.</note>
      <note>Annotation on Thomason copy: "Dec 5 1648".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- I, -- King of England, 1600-1649 -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A78782</ep:tcp>
    <ep:estc/>
    <ep:stc> (Thomason 669.f.13[51]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>His Majesties farevvel speech unto the Lords Commissioners at Newport in the Isle of Wight.:</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1648</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>243</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A78782-e10">
  <body xml:id="A78782-e20">
   <pb facs="tcp:162947:1" rend="simple:additions" xml:id="A78782-001-a"/>
   <div type="speech" xml:id="A78782-e30">
    <head xml:id="A78782-e40">
     <figure xml:id="A78782-e50">
      <p xml:id="A78782-e60">
       <w lemma="c" pos="sy" xml:id="A78782-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A78782-001-a-0020">R</w>
      </p>
      <p xml:id="A78782-e70">
       <w lemma="n/a" pos="ffr" reg="Dieu" xml:id="A78782-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A78782-e80">
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A78782-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A78782-e90">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A78782-e100">
     <w lemma="his" pos="po" xml:id="A78782-001-a-0130">HIS</w>
     <w lemma="majesty" pos="n2" reg="majesties" xml:id="A78782-001-a-0140">MAIESTIES</w>
     <w lemma="farewell" orig="FAREVVEL" pos="uh" xml:id="A78782-001-a-0150">FAREWEL</w>
     <w lemma="speech" pos="n1" xml:id="A78782-001-a-0160">SPEECH</w>
     <w lemma="unto" pos="acp" reg="unto" xml:id="A78782-001-a-0170">Vnto</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-0180">the</w>
     <w lemma="lord" pos="n2" xml:id="A78782-001-a-0190">Lords</w>
     <w lemma="commissioner" pos="n2" xml:id="A78782-001-a-0200">Commissioners</w>
     <w lemma="at" pos="acp" xml:id="A78782-001-a-0210">at</w>
     <w lemma="Newport" pos="nn1" rend="hi" xml:id="A78782-001-a-0220">Newport</w>
     <w lemma="in" pos="acp" xml:id="A78782-001-a-0230">in</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-0240">the</w>
     <w lemma="isle" pos="n1" xml:id="A78782-001-a-0250">Isle</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-0260">of</w>
     <hi xml:id="A78782-e120">
      <w lemma="wight" pos="nn1" xml:id="A78782-001-a-0270">Wight</w>
      <pc unit="sentence" xml:id="A78782-001-a-0280">.</pc>
     </hi>
    </head>
    <opener xml:id="A78782-e130">
     <salute xml:id="A78782-e140">
      <w lemma="my" pos="po" rend="decorinit" xml:id="A78782-001-a-0290">MY</w>
      <hi xml:id="A78782-e150">
       <w lemma="lord" pos="n2" xml:id="A78782-001-a-0300">LORDS</w>
       <pc xml:id="A78782-001-a-0310">,</pc>
      </hi>
     </salute>
    </opener>
    <p xml:id="A78782-e160">
     <w lemma="you" pos="pn" xml:id="A78782-001-a-0320">You</w>
     <w lemma="be" pos="vvb" xml:id="A78782-001-a-0330">are</w>
     <w lemma="come" pos="vvn" xml:id="A78782-001-a-0340">come</w>
     <w lemma="to" pos="prt" xml:id="A78782-001-a-0350">to</w>
     <w lemma="take" pos="vvi" xml:id="A78782-001-a-0360">take</w>
     <w lemma="your" pos="po" xml:id="A78782-001-a-0370">your</w>
     <w lemma="leave" pos="n1" xml:id="A78782-001-a-0380">leave</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-0390">of</w>
     <w lemma="i" pos="pno" xml:id="A78782-001-a-0400">me</w>
     <pc xml:id="A78782-001-a-0410">,</pc>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-0420">and</w>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-0430">I</w>
     <w lemma="believe" pos="vvb" reg="believe" xml:id="A78782-001-a-0440">beleeve</w>
     <w lemma="we" pos="pns" xml:id="A78782-001-a-0450">we</w>
     <w lemma="shall" pos="vmb" xml:id="A78782-001-a-0460">shall</w>
     <w lemma="scarce" pos="av-j" xml:id="A78782-001-a-0470">scarce</w>
     <w lemma="ever" pos="av" xml:id="A78782-001-a-0480">ever</w>
     <w lemma="see" pos="vvi" xml:id="A78782-001-a-0490">see</w>
     <w lemma="each" pos="d" xml:id="A78782-001-a-0500">each</w>
     <w lemma="other" pos="pi-d" xml:id="A78782-001-a-0510">other</w>
     <w lemma="again" pos="av" reg="again" xml:id="A78782-001-a-0520">againe</w>
     <pc xml:id="A78782-001-a-0530">:</pc>
     <w lemma="but" pos="acp" xml:id="A78782-001-a-0540">but</w>
     <w lemma="god" pos="nng1" reg="God's" xml:id="A78782-001-a-0550">Gods</w>
     <w lemma="will" pos="vmb" xml:id="A78782-001-a-0560">will</w>
     <w lemma="be" pos="vvi" xml:id="A78782-001-a-0570">be</w>
     <w lemma="do" pos="vvn" xml:id="A78782-001-a-0580">done</w>
     <pc xml:id="A78782-001-a-0590">,</pc>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-0600">I</w>
     <w lemma="thank" pos="vvb" reg="thank" xml:id="A78782-001-a-0610">thanke</w>
     <w lemma="God" pos="nn1" xml:id="A78782-001-a-0620">God</w>
     <pc xml:id="A78782-001-a-0630">,</pc>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-0640">I</w>
     <w lemma="have" pos="vvb" xml:id="A78782-001-a-0650">have</w>
     <w lemma="make" pos="vvn" xml:id="A78782-001-a-0660">made</w>
     <w lemma="my" pos="po" xml:id="A78782-001-a-0670">my</w>
     <w lemma="peace" pos="n1" xml:id="A78782-001-a-0680">peace</w>
     <w lemma="with" pos="acp" xml:id="A78782-001-a-0690">with</w>
     <w lemma="he" pos="pno" xml:id="A78782-001-a-0700">him</w>
     <pc xml:id="A78782-001-a-0710">,</pc>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-0720">and</w>
     <w lemma="shall" pos="vmb" xml:id="A78782-001-a-0730">shall</w>
     <w lemma="without" pos="acp" xml:id="A78782-001-a-0740">without</w>
     <w lemma="fear" pos="n1" reg="fear" xml:id="A78782-001-a-0750">feare</w>
     <pc xml:id="A78782-001-a-0760">,</pc>
     <w lemma="undergo" pos="vvb" reg="undergo" xml:id="A78782-001-a-0770">undergoe</w>
     <w lemma="what" pos="crq" xml:id="A78782-001-a-0780">what</w>
     <w lemma="he" pos="pns" xml:id="A78782-001-a-0790">he</w>
     <w lemma="shall" pos="vmb" xml:id="A78782-001-a-0800">shall</w>
     <w lemma="be" pos="vvi" xml:id="A78782-001-a-0810">be</w>
     <w lemma="please" pos="vvn" xml:id="A78782-001-a-0820">pleased</w>
     <w lemma="to" pos="prt" xml:id="A78782-001-a-0830">to</w>
     <w lemma="suffer" pos="vvi" xml:id="A78782-001-a-0840">suffer</w>
     <w lemma="man" pos="n2" xml:id="A78782-001-a-0850">men</w>
     <w lemma="to" pos="prt" xml:id="A78782-001-a-0860">to</w>
     <w lemma="do" pos="vvi" reg="do" xml:id="A78782-001-a-0870">doe</w>
     <w lemma="unto" pos="acp" xml:id="A78782-001-a-0880">unto</w>
     <w lemma="i" pos="pno" xml:id="A78782-001-a-0890">me</w>
     <pc unit="sentence" xml:id="A78782-001-a-0900">.</pc>
    </p>
    <p xml:id="A78782-e170">
     <w lemma="my" pos="po" xml:id="A78782-001-a-0910">My</w>
     <w lemma="lord" pos="n2" xml:id="A78782-001-a-0920">Lords</w>
     <pc xml:id="A78782-001-a-0930">,</pc>
     <w lemma="you" pos="pn" xml:id="A78782-001-a-0940">you</w>
     <w lemma="can" pos="vmbx" xml:id="A78782-001-a-0950">cannot</w>
     <w lemma="but" pos="acp" xml:id="A78782-001-a-0960">but</w>
     <w lemma="know" pos="vvi" xml:id="A78782-001-a-0970">know</w>
     <pc xml:id="A78782-001-a-0980">,</pc>
     <w lemma="that" pos="cs" xml:id="A78782-001-a-0990">that</w>
     <w lemma="in" pos="acp" xml:id="A78782-001-a-1000">in</w>
     <w lemma="my" pos="po" xml:id="A78782-001-a-1010">my</w>
     <w lemma="fall" pos="n1" xml:id="A78782-001-a-1020">fall</w>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1030">and</w>
     <w lemma="ruin" pos="n1" reg="ruin" xml:id="A78782-001-a-1040">ruine</w>
     <pc xml:id="A78782-001-a-1050">,</pc>
     <w lemma="you" pos="pn" xml:id="A78782-001-a-1060">you</w>
     <w lemma="see" pos="vvb" xml:id="A78782-001-a-1070">see</w>
     <w lemma="your" pos="po" xml:id="A78782-001-a-1080">your</w>
     <w lemma="own" pos="d" reg="own" xml:id="A78782-001-a-1090">owne</w>
     <pc xml:id="A78782-001-a-1100">,</pc>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1110">and</w>
     <w lemma="that" pos="cs" xml:id="A78782-001-a-1120">that</w>
     <w lemma="also" pos="av" xml:id="A78782-001-a-1130">also</w>
     <w lemma="near" pos="acp" reg="near" xml:id="A78782-001-a-1140">neere</w>
     <w lemma="to" pos="acp" xml:id="A78782-001-a-1150">to</w>
     <w lemma="you" pos="pn" xml:id="A78782-001-a-1160">you</w>
     <pc xml:id="A78782-001-a-1170">;</pc>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-1180">I</w>
     <w lemma="pray" pos="vvb" xml:id="A78782-001-a-1190">pray</w>
     <w lemma="God" pos="nn1" xml:id="A78782-001-a-1200">God</w>
     <w lemma="send" pos="vvb" xml:id="A78782-001-a-1210">send</w>
     <w lemma="you" pos="pn" xml:id="A78782-001-a-1220">you</w>
     <w lemma="better" pos="avc-j" xml:id="A78782-001-a-1230">better</w>
     <w lemma="friend" pos="n2" xml:id="A78782-001-a-1240">friends</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A78782-001-a-1250">then</w>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-1260">I</w>
     <w lemma="have" pos="vvb" xml:id="A78782-001-a-1270">have</w>
     <w lemma="find" pos="vvn" xml:id="A78782-001-a-1280">found</w>
     <pc unit="sentence" xml:id="A78782-001-a-1290">.</pc>
    </p>
    <p xml:id="A78782-e180">
     <w lemma="I" pos="pns" xml:id="A78782-001-a-1300">I</w>
     <w lemma="be" pos="vvm" xml:id="A78782-001-a-1310">am</w>
     <w lemma="full" pos="av-j" xml:id="A78782-001-a-1320">fully</w>
     <w lemma="inform" pos="vvn" xml:id="A78782-001-a-1330">informed</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-1340">of</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-1350">the</w>
     <w lemma="whole" pos="j" xml:id="A78782-001-a-1360">whole</w>
     <w lemma="carriage" pos="n1" reg="carriage" xml:id="A78782-001-a-1370">cariage</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-1380">of</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-1390">the</w>
     <w lemma="plot" pos="n1" xml:id="A78782-001-a-1400">plot</w>
     <w lemma="against" pos="acp" xml:id="A78782-001-a-1410">against</w>
     <w lemma="i" pos="pno" xml:id="A78782-001-a-1420">me</w>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1430">and</w>
     <w lemma="i" pos="png" xml:id="A78782-001-a-1440">mine</w>
     <pc xml:id="A78782-001-a-1450">;</pc>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1460">and</w>
     <w lemma="nothing" pos="pix" xml:id="A78782-001-a-1470">nothing</w>
     <w lemma="so" pos="av" xml:id="A78782-001-a-1480">so</w>
     <w lemma="much" pos="av-d" xml:id="A78782-001-a-1490">much</w>
     <w lemma="afflict" pos="vvz" xml:id="A78782-001-a-1500">afflicts</w>
     <w lemma="i" pos="pno" xml:id="A78782-001-a-1510">me</w>
     <pc xml:id="A78782-001-a-1520">,</pc>
     <w lemma="as" pos="acp" xml:id="A78782-001-a-1530">as</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-1540">the</w>
     <w lemma="sense" pos="n1" reg="sense" xml:id="A78782-001-a-1550">sence</w>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1560">and</w>
     <w lemma="feeling" pos="n1" xml:id="A78782-001-a-1570">feeling</w>
     <w lemma="I" pos="pns" xml:id="A78782-001-a-1580">I</w>
     <w lemma="have" pos="vvb" xml:id="A78782-001-a-1590">have</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-1600">of</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-1610">the</w>
     <w lemma="suffering" pos="n2" xml:id="A78782-001-a-1620">sufferings</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-1630">of</w>
     <w lemma="my" pos="po" xml:id="A78782-001-a-1640">my</w>
     <w lemma="subject" pos="n2" xml:id="A78782-001-a-1650">Subjects</w>
     <pc xml:id="A78782-001-a-1660">,</pc>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1670">and</w>
     <w lemma="the" pos="d" xml:id="A78782-001-a-1680">the</w>
     <w lemma="misery" pos="n2" xml:id="A78782-001-a-1690">miseries</w>
     <w lemma="that" pos="cs" xml:id="A78782-001-a-1700">that</w>
     <w lemma="hang" pos="vvb" xml:id="A78782-001-a-1710">hang</w>
     <w lemma="over" pos="acp" xml:id="A78782-001-a-1720">over</w>
     <w lemma="my" pos="po" xml:id="A78782-001-a-1730">my</w>
     <w lemma="three" pos="crd" xml:id="A78782-001-a-1740">three</w>
     <w lemma="kingdom" pos="n2" reg="kingdoms" xml:id="A78782-001-a-1750">Kingdomes</w>
     <pc xml:id="A78782-001-a-1760">,</pc>
     <w lemma="draw" pos="vvn" reg="drawn" xml:id="A78782-001-a-1770">drawne</w>
     <w lemma="upon" pos="acp" xml:id="A78782-001-a-1780">upon</w>
     <w lemma="they" pos="pno" xml:id="A78782-001-a-1790">them</w>
     <w lemma="by" pos="acp" xml:id="A78782-001-a-1800">by</w>
     <w lemma="those" pos="d" xml:id="A78782-001-a-1810">those</w>
     <w lemma="who" pos="crq" xml:id="A78782-001-a-1820">who</w>
     <pc join="right" xml:id="A78782-001-a-1830">(</pc>
     <w lemma="upon" pos="acp" xml:id="A78782-001-a-1840">upon</w>
     <w lemma="pretence" pos="n2" xml:id="A78782-001-a-1850">pretences</w>
     <w lemma="of" pos="acp" xml:id="A78782-001-a-1860">of</w>
     <w lemma="good" pos="j" xml:id="A78782-001-a-1870">good</w>
     <pc xml:id="A78782-001-a-1880">)</pc>
     <w lemma="violent" pos="av-j" xml:id="A78782-001-a-1890">violently</w>
     <w lemma="pursue" pos="vvi" xml:id="A78782-001-a-1900">pursue</w>
     <w lemma="their" pos="po" xml:id="A78782-001-a-1910">their</w>
     <w lemma="own" pos="d" reg="own" xml:id="A78782-001-a-1920">owne</w>
     <w lemma="interest" pos="n2" xml:id="A78782-001-a-1930">interests</w>
     <w lemma="and" pos="cc" xml:id="A78782-001-a-1940">and</w>
     <w lemma="end" pos="n2" xml:id="A78782-001-a-1950">ends</w>
     <pc unit="sentence" xml:id="A78782-001-a-1960">.</pc>
    </p>
    <p xml:id="A78782-e190">
     <hi xml:id="A78782-e200">
      <w lemma="these" pos="d" xml:id="A78782-001-a-1970">These</w>
      <w lemma="word" pos="n2" xml:id="A78782-001-a-1980">words</w>
      <w lemma="his" pos="po" xml:id="A78782-001-a-1990">His</w>
      <w lemma="majesty" pos="n1" xml:id="A78782-001-a-2000">Majesty</w>
      <w lemma="deliver" pos="vvn" xml:id="A78782-001-a-2010">delivered</w>
      <w lemma="with" pos="acp" xml:id="A78782-001-a-2020">with</w>
      <w lemma="much" pos="d" xml:id="A78782-001-a-2030">much</w>
      <w lemma="alacrity" pos="n1" xml:id="A78782-001-a-2040">alacrity</w>
      <pc xml:id="A78782-001-a-2050">,</pc>
      <w lemma="and" pos="cc" xml:id="A78782-001-a-2060">and</w>
      <w lemma="cheerfulness" pos="n1" reg="cheerfulness" xml:id="A78782-001-a-2070">cheerfulnesse</w>
      <pc xml:id="A78782-001-a-2080">,</pc>
      <w lemma="with" pos="acp" xml:id="A78782-001-a-2090">with</w>
      <w lemma="a" pos="d" xml:id="A78782-001-a-2100">a</w>
      <w lemma="serene" pos="j" xml:id="A78782-001-a-2110">serene</w>
      <w lemma="countenance" pos="n1" xml:id="A78782-001-a-2120">Countenance</w>
      <pc xml:id="A78782-001-a-2130">,</pc>
      <w lemma="and" pos="cc" xml:id="A78782-001-a-2140">and</w>
      <w lemma="a" pos="d" xml:id="A78782-001-a-2150">a</w>
      <w lemma="carriage" pos="n1" reg="carriage" xml:id="A78782-001-a-2160">cariage</w>
      <w lemma="free" pos="j" xml:id="A78782-001-a-2170">free</w>
      <w lemma="from" pos="acp" xml:id="A78782-001-a-2180">from</w>
      <w lemma="all" pos="d" xml:id="A78782-001-a-2190">all</w>
      <w lemma="disturbance" pos="n1" xml:id="A78782-001-a-2200">disturbance</w>
      <pc unit="sentence" xml:id="A78782-001-a-2210">.</pc>
     </hi>
    </p>
    <p xml:id="A78782-e210">
     <hi xml:id="A78782-e220">
      <w lemma="thus" pos="av" xml:id="A78782-001-a-2220">Thus</w>
      <w lemma="part" pos="vvg" xml:id="A78782-001-a-2230">parting</w>
      <w lemma="with" pos="acp" xml:id="A78782-001-a-2240">with</w>
      <w lemma="the" pos="d" xml:id="A78782-001-a-2250">the</w>
      <w lemma="lord" pos="n2" xml:id="A78782-001-a-2260">Lords</w>
      <pc xml:id="A78782-001-a-2270">,</pc>
      <w lemma="leave" pos="vvg" xml:id="A78782-001-a-2280">leaving</w>
      <w lemma="many" pos="d" xml:id="A78782-001-a-2290">many</w>
      <w lemma="tender" pos="j" xml:id="A78782-001-a-2300">tender</w>
      <w lemma="impression" pos="n2" xml:id="A78782-001-a-2310">Impressions</w>
      <pc xml:id="A78782-001-a-2320">,</pc>
      <pc join="right" xml:id="A78782-001-a-2330">(</pc>
      <w lemma="if" pos="cs" xml:id="A78782-001-a-2340">if</w>
      <w lemma="not" pos="xx" xml:id="A78782-001-a-2350">not</w>
      <w lemma="in" pos="acp" xml:id="A78782-001-a-2360">in</w>
      <w lemma="they" pos="pno" xml:id="A78782-001-a-2370">them</w>
      <pc xml:id="A78782-001-a-2380">)</pc>
      <w lemma="in" pos="acp" xml:id="A78782-001-a-2390">in</w>
      <w lemma="the" pos="d" xml:id="A78782-001-a-2400">the</w>
      <w lemma="other" pos="d" xml:id="A78782-001-a-2410">other</w>
      <w lemma="hearer" pos="n2" xml:id="A78782-001-a-2420">hearers</w>
      <pc unit="sentence" xml:id="A78782-001-a-2430">.</pc>
     </hi>
    </p>
   </div>
  </body>
 </text>
</TEI>
